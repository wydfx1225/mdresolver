// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from "./store/store"
import MinioClient from "./assets/minio/MinioClient"
import Confirm from 'vue-confirm'
import mavonEditor from 'mavon-editor'
// import 'mavon-editor/dist/css/index.css'
import axios from "axios";
import Util from "./assets/utils/Util"

Vue.config.productionTip = false
Vue.use(Confirm)
Vue.config.productionTip = false
Vue.use(mavonEditor)
Vue.prototype.$https = axios;
Vue.prototype.utils = Util;
Vue.prototype.$minio = new MinioClient();
axios.defaults.baseURL = "";

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  store
})
