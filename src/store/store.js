import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

const state = {
    test: 'test'
}
const mutations = {
  testMutation(){
    console.log('testMutation')
  }
}
export default new Vuex.Store({
    state: state,
    mutations: mutations
})
